import numpy as np
import matplotlib.pyplot as plt
import math

def rk4s(f,h,x):
    k1 = h*f(x)
    k2 = h*f(x+0.5*k1)
    k3 = h*f(x+0.5*k2)
    k4 = h*f(x+k3)
    x += (k1+2*k2+2*k3+k4)/6
    return x

def getGrid(lims,kx,ky=None):
    if ky is None:ky = int(kx*np.diff(lims[2:])/np.diff(lims[:2]))
    points = []
    for x in np.linspace(lims[0],lims[1],num=kx, endpoint=True):
        for y in np.linspace(lims[2],lims[3],num=ky, endpoint=True):
            points.append((x,y))
    return points

def getHex(x,y,R,k,center=True):
    R = R/k
    points=[(x,y)] if center else []
    angle = [i*math.pi/3 for i in range(6)]
    dx = [ R*(math.cos(angle[(i+1)%6])-math.cos(angle[i])) for i in range(6) ]
    dy = [ R*(math.sin(angle[(i+1)%6])-math.sin(angle[i])) for i in range(6) ]
    for r in range(1,k):
        for a in range(6):
            xn = x+r*R*math.cos(angle[a])
            yn = y+r*R*math.sin(angle[a])
            points.append((xn,yn))
            for e in range(1,r):
                points.append((xn+e*dx[a],yn+e*dy[a]))
    return points

def getLine(xs,ys,dx,dy,k):
    return [(xs+i*dx/k,ys+i*dy/k) for i in range(k)]


def PlotSpace(f, lims, points, L, size, title='', frame='output', bounded=True,
              poi=[], xl=[], yl=[], ms=2, pms=8, marker='o', dpi=60, trans_alpha=0.2):
    F = np.zeros(2)
    plt.figure(figsize=size).gca().set_title(title,fontsize=16)
    plt.xlim(lims[0],lims[1])
    plt.ylim(lims[2],lims[3])
    for (x,y) in points:
        X1 = [x,y]
        plt.plot(x,y, marker ='o', c='k', ms=ms)
        for it in range(L[0]):
            if (bounded):
                if ( (X1[0]<lims[0]) or (X1[0]>lims[1]) or
                     (X1[1]<lims[2]) or (X1[1]>lims[3])): break
            X2 = rk4s(f,L[1],X1)
            plt.plot([X1[0],X2[0]],[X1[1],X2[1]],
                     marker ='.', c='r', alpha=(1 if (it>=L[2]) else trans_alpha),ms=0, lw=0.3)
            X1[:] = X2[:]
    for (x,y) in poi: plt.plot(x,y, marker=marker, c='b', ms=pms)
    for x in xl: plt.axvline(x=x,ls='--',c='k',alpha=.5)
    for y in yl: plt.axhline(y=y,ls='--',c='k',alpha=.5)
    plt.savefig('frames/'+str(frame)+'.png', format='png', dpi=dpi, bbox_inches='tight')
    plt.close()
